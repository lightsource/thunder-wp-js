export default {
    exists: function (value, name) {
        if (value) {
            return;
        }
        throw new Error('Dependency is missing : ' + name);
    },
};
