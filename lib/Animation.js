class CssAnimation {
    constructor(settings = {}) {
        this.settings = settings;
        'loading' === document.readyState ?
            document.addEventListener('DOMContentLoaded', this.setup.bind(this))
            : this.setup();
    }
    applyAnimation(parent) {
        parent.querySelectorAll('.animation-item').forEach((element) => {
            element.classList.add('animation-item--active');
        });
    }
    intersection(entries, observer) {
        entries.forEach((entry) => {
            if (!entry.isIntersecting) {
                return;
            }
            let parent = entry.target;
            this.applyAnimation(parent);
            observer.unobserve(parent);
        });
    }
    setup() {
        let options = Object.assign({
            root: null,
            rootMargin: "0px",
            threshold: .4,
        }, this.settings);
        let observer = new IntersectionObserver(this.intersection.bind(this), options);
        document.querySelectorAll('.animation-parent').forEach((element) => {
            observer.observe(element);
        });
    }
}
export default CssAnimation;
