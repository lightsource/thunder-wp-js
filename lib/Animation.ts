class CssAnimation {
    private settings: object

    constructor(settings: object = {}) {
        this.settings = settings

        'loading' === document.readyState ?
            document.addEventListener('DOMContentLoaded', this.setup.bind(this))
            : this.setup();
    }

    private applyAnimation(parent: Element): void {
        parent.querySelectorAll('.animation-item').forEach((element: HTMLElement) => {
            element.classList.add('animation-item--active')
        })
    }

    public intersection(entries, observer): void {
        entries.forEach((entry) => {
            if (!entry.isIntersecting) {
                return
            }

            let parent = entry.target

            this.applyAnimation(parent)

            observer.unobserve(parent)
        })
    }

    public setup(): void {
        let options = Object.assign({
            root: null,
            rootMargin: "0px",
            threshold: .4,
        }, this.settings);

        let observer = new IntersectionObserver(this.intersection.bind(this), options);

        document.querySelectorAll('.animation-parent').forEach((element) => {
            observer.observe(element)
        })
    }
}

export default CssAnimation
