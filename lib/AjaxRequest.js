import dependency from "./Dependency";
let logger = window._Logger;
dependency.exists(logger, '_Logger');
class Setting {
    constructor() {
        this.url = '';
        this.name = '';
        this.type = 'POST';
        this.isCustomResponse = false;
    }
}
class AjaxRequest {
    constructor(settings) {
        this.settings = settings;
    }
    parseResponse(response) {
        let result = null;
        try {
            result = JSON.parse(response);
            if (!result.hasOwnProperty('isSuccessful') ||
                !result.hasOwnProperty('errors') ||
                !result.hasOwnProperty('html')) {
                new Error('Properties are missing');
            }
        }
        catch (exception) {
            logger.write(logger.level.ERROR, 'Fail to parse JSON', {
                error: exception,
                response: response,
            });
            new Error('Fail to parse json');
        }
        return result;
    }
    processResponse(request, responseHandler) {
        if (request.readyState !== 4) {
            return;
        }
        if (200 !== request.status) {
            logger.write(logger.level.ERROR, 'Response status is wrong', {
                request: request,
            });
            new Error('Response status is wrong');
        }
        if (this.settings.isCustomResponse) {
            !responseHandler || responseHandler(request.responseText);
            return;
        }
        let response = this.parseResponse(request.responseText);
        if (!response.isSuccessful) {
            logger.write(logger.level.ERROR, 'Request was wrong', {
                response: response,
            });
            new Error('Request was wrong');
        }
        !responseHandler || responseHandler(response.html);
    }
    make(data = null, responseHandler = null) {
        let formData = new FormData();
        !this.settings.name || formData.append('action', this.settings.name);
        formData.append('_noCache', new Date().getTime().toString());
        if (data) {
            for (let property in data) {
                if (!data.hasOwnProperty(property)) {
                    continue;
                }
                formData.append(property.toString(), data[property].toString());
            }
        }
        const request = new XMLHttpRequest();
        request.timeout = 10000;
        request.open(this.settings.type, this.settings.url, true);
        request.addEventListener('readystatechange', this.processResponse.bind(this, request, responseHandler));
        request.addEventListener('timeout', () => {
            throw new Error('Ajax timeout');
        });
        request.send(formData);
    }
}
window['_AjaxRequest'] = {
    Settings: Setting,
    Request: AjaxRequest,
};
