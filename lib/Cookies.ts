import Cookies from 'js-cookie'

declare global {
    interface Window {
        _Cookies: any,
    }
}

window['_Cookies'] = Cookies
