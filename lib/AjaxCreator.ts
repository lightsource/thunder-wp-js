import dependency from "./Dependency";

let ajaxRequest = window._AjaxRequest
let logger = window._Logger

dependency.exists(ajaxRequest, '_AjaxRequest')
dependency.exists(logger, '_Log')

class AjaxCreator {
    public create(
        blockName: string,
        settings: InstanceType<typeof ajaxRequest.Settings>,
    ): InstanceType<typeof ajaxRequest.Request> {

        if (window.hasOwnProperty('acf_blocks') &&
            window.acf_blocks.hasOwnProperty(blockName) &&
            window.acf_blocks[blockName].hasOwnProperty('ajax') &&
            window.acf_blocks[blockName].ajax.hasOwnProperty('URL') &&
            window.acf_blocks[blockName].ajax.hasOwnProperty('NAME')) {
            if (!settings.url) {
                settings.url = window.acf_blocks[blockName].ajax.URL
            }

            settings.name = window.acf_blocks[blockName].ajax.NAME
        }

        if (!settings.url ||
            !settings.name) {
            logger.write(logger.level.ERROR, 'Required data for ajaxCreator is missing', {
                blockName: blockName,
            })

            throw new Error('Required data for ajaxCreator is missing')
        }

        return new ajaxRequest.Request(settings)
    }
}


declare global {
    interface Window {
        acf_blocks: any,
        _AjaxCreator: typeof AjaxCreator,
    }
}

window['_AjaxCreator'] = AjaxCreator
