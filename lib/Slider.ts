import Splide from '@splidejs/splide'

class SliderSettings {
    public isLoop: boolean
    public perPage: number
    public perMove: number
    // e.g. '80px'
    public padding: string
    public isHasArrows: boolean
    public isHasDots: boolean
    // e.g. '25px'
    public itemMargin: string
    // dynamic height of the slider deps on every slide
    public isAdaptiveHeight: boolean
    public multiplyQuantity: number
    public customType: string
    public updateOnMove: boolean
    public arrowPath: string

    public constructor() {
        this.isLoop = true
        this.perPage = 1
        this.perMove = 1
        this.padding = ''
        this.isHasArrows = false
        this.isHasDots = false
        this.isAdaptiveHeight = false
        this.customType = ''
        // default is false, but we use true
        // because it's necessary for any animation related to the active item
        this.updateOnMove = true
        this.arrowPath = ''
    }

    public getSplideSettings(): any {
        let type = this.isLoop ?
            'loop' :
            'slide'
        type = this.customType || type

        let settings: any = {
            perPage: this.perPage,
            perMove: this.perMove,
            padding: this.padding,
            type: type,
            arrows: this.isHasArrows,
            pagination: this.isHasDots,
            // don't need to have a separate option, because also there is a height transition which hardcoded in css
            speed: 600,
            gap: this.itemMargin,
            // add height animation, also duplicate of the height animation there is in css, to cover all cases
            easing: 'cubic-bezier(0.25, 1, 0.5, 1), height .6s ease',
            // special option to enable loop for 'fade' slider type
            rewind: 'fade' === type && this.isLoop,
            updateOnMove: this.updateOnMove,
        }

        if (this.arrowPath) {
            settings.arrowPath = this.arrowPath
        }

        return settings
    }
}

class Settings {
    public isDestroyOnDesktop: boolean
    public isDestroyOnMobile: boolean

    public desktop: SliderSettings
    public mobile: SliderSettings
    public desktopSinceWidth: number = 992

    public constructor() {

        this.isDestroyOnDesktop = false
        this.isDestroyOnMobile = false

        this.desktop = new SliderSettings()
        this.mobile = new SliderSettings()
    }

    public getSplideSettings(): any {

        let splideSettings = this.desktop.getSplideSettings()

        // breakpoints field has desktop first order
        splideSettings.breakpoints = {}

        // disable random drag on desktop
        splideSettings.breakpoints[10000] = {
            drag: false,
        }
        splideSettings.breakpoints[this.desktopSinceWidth] = {
            drag: true,
        }

        // specific destroy
        if (this.isDestroyOnDesktop ||
            this.isDestroyOnMobile) {
            // bool keeps listeners for remount if window size is changing, 'completely' removes all
            splideSettings.breakpoints[10000].destroy = this.isDestroyOnDesktop
            splideSettings.breakpoints[this.desktopSinceWidth].destroy = !this.isDestroyOnDesktop
        }

        // mobile settings
        splideSettings.breakpoints[this.desktopSinceWidth] = Object.assign(splideSettings.breakpoints[this.desktopSinceWidth],
            this.mobile.getSplideSettings())

        return splideSettings
    }

    public getCurrentSliderSettings(): SliderSettings {
        return window.innerWidth < this.desktopSinceWidth ?
            this.mobile :
            this.desktop
    }
}

class LazyInit {

    private intersectionObserver: IntersectionObserver | null
    private options: any
    private sliders: Slider[]

    constructor() {

        this.options = {
            root: null,
            rootMargin: 500 + 'px' + ' 0px',
            threshold: 0.01,
        }
        this.sliders = []

        if (!window.hasOwnProperty('IntersectionObserver')) {
            console.log('LazyLoading failed, browser doesn\'t support IntersectionObserver')
            return
        }

        this.intersectionObserver = new IntersectionObserver(this.mountIntersectedSliders.bind(this), this.options)

    }

    public mountIntersectedSliders(entries, observer) {
        entries.forEach(entry => {
            if (!entry.isIntersecting) {
                return
            }
            let target = entry.target

            this.sliders.forEach((slider) => {
                if (slider.getElement() !== target) {
                    return
                }

                slider.lazyMount()
            })

            observer.unobserve(target)
        })

    }

    public addSlider(slider: Slider): void {
        if (!this.intersectionObserver) {
            slider.lazyMount()
            return
        }

        this.sliders.push(slider)
        this.intersectionObserver.observe(slider.getElement())
    }

}

let lazyInit = new LazyInit()

class Slider {

    private slider: Splide
    private element: HTMLElement
    private settings: Settings

    constructor(element: HTMLElement, settings: Settings) {
        this.element = element
        this.settings = settings
        this.slider = new Splide(element, settings.getSplideSettings())
    }

    private isHaveToInit(): boolean {
        let countOfItems = this.element.querySelectorAll('.splide__item').length
        let multiplyQuantity = this.settings.getCurrentSliderSettings().multiplyQuantity

        return (!multiplyQuantity ||
            !!(countOfItems % multiplyQuantity))
    }

    // with css will solve the different slides height issue
    private updateSlideHeightAccordingToSlide(slideIndex: number): void {
        let index = slideIndex < 10 ?
            '0' + slideIndex :
            slideIndex.toString()

        let id = this.element.getAttribute('id')
        let nextSlide: HTMLElement = this.element.querySelector('#' + id + '-slide' + index);

        (<HTMLElement>this.element.querySelector('.splide__list')).style.height = nextSlide.scrollHeight + 'px'
    }

    public goToNextSlide(): void {
        this.slider.go('>')
    }

    public goToPrevSlide(): void {
        this.slider.go('<')
    }

    // from 0
    public goToSlide(slideNumber: number): void {
        this.slider.go(slideNumber)
    }

    public addInitListener(callback): void {
        this.slider.on('mounted', callback)
    }

    public mount(isLazy: boolean = true): void {
        isLazy ?
            lazyInit.addSlider(this) :
            this.lazyMount()
    }

    public lazyMount() {
        this.slider.on('mounted', () => {
            if (!this.settings.getCurrentSliderSettings().isAdaptiveHeight) {
                return
            }

            this.updateSlideHeightAccordingToSlide(1)
        })

        // don't skip mount, even if should destroy later
        // because should be initialized, like '.is-initialized' class and display items, etc..
        this.slider.mount()

        this.slider.on('move', (newIndex: number, oldIndex: number, destIndex: number) => {
            if (!this.settings.getCurrentSliderSettings().isAdaptiveHeight) {
                return
            }

            this.updateSlideHeightAccordingToSlide(newIndex + 1)
        })

        if (!this.isHaveToInit()) {
            this.slider.destroy(true)
            this.element.classList.add('an-skipped')
        }
    }

    public getElement(): HTMLElement {
        return this.element
    }
}

declare global {
    interface Window {
        _Slider: {
            Slider: typeof Slider,
            Settings: typeof Settings,
        }
    }
}

window['_Slider'] = {
    Slider: Slider,
    Settings: Settings,
}
