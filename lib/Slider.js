import Splide from '@splidejs/splide';
class SliderSettings {
    constructor() {
        this.isLoop = true;
        this.perPage = 1;
        this.perMove = 1;
        this.padding = '';
        this.isHasArrows = false;
        this.isHasDots = false;
        this.isAdaptiveHeight = false;
        this.customType = '';
        // default is false, but we use true
        // because it's necessary for any animation related to the active item
        this.updateOnMove = true;
        this.arrowPath = '';
    }
    getSplideSettings() {
        let type = this.isLoop ?
            'loop' :
            'slide';
        type = this.customType || type;
        let settings = {
            perPage: this.perPage,
            perMove: this.perMove,
            padding: this.padding,
            type: type,
            arrows: this.isHasArrows,
            pagination: this.isHasDots,
            // don't need to have a separate option, because also there is a height transition which hardcoded in css
            speed: 600,
            gap: this.itemMargin,
            // add height animation, also duplicate of the height animation there is in css, to cover all cases
            easing: 'cubic-bezier(0.25, 1, 0.5, 1), height .6s ease',
            // special option to enable loop for 'fade' slider type
            rewind: 'fade' === type && this.isLoop,
            updateOnMove: this.updateOnMove,
        };
        if (this.arrowPath) {
            settings.arrowPath = this.arrowPath;
        }
        return settings;
    }
}
class Settings {
    constructor() {
        this.desktopSinceWidth = 992;
        this.isDestroyOnDesktop = false;
        this.isDestroyOnMobile = false;
        this.desktop = new SliderSettings();
        this.mobile = new SliderSettings();
    }
    getSplideSettings() {
        let splideSettings = this.desktop.getSplideSettings();
        // breakpoints field has desktop first order
        splideSettings.breakpoints = {};
        // disable random drag on desktop
        splideSettings.breakpoints[10000] = {
            drag: false,
        };
        splideSettings.breakpoints[this.desktopSinceWidth] = {
            drag: true,
        };
        // specific destroy
        if (this.isDestroyOnDesktop ||
            this.isDestroyOnMobile) {
            // bool keeps listeners for remount if window size is changing, 'completely' removes all
            splideSettings.breakpoints[10000].destroy = this.isDestroyOnDesktop;
            splideSettings.breakpoints[this.desktopSinceWidth].destroy = !this.isDestroyOnDesktop;
        }
        // mobile settings
        splideSettings.breakpoints[this.desktopSinceWidth] = Object.assign(splideSettings.breakpoints[this.desktopSinceWidth], this.mobile.getSplideSettings());
        return splideSettings;
    }
    getCurrentSliderSettings() {
        return window.innerWidth < this.desktopSinceWidth ?
            this.mobile :
            this.desktop;
    }
}
class LazyInit {
    constructor() {
        this.options = {
            root: null,
            rootMargin: 500 + 'px' + ' 0px',
            threshold: 0.01,
        };
        this.sliders = [];
        if (!window.hasOwnProperty('IntersectionObserver')) {
            console.log('LazyLoading failed, browser doesn\'t support IntersectionObserver');
            return;
        }
        this.intersectionObserver = new IntersectionObserver(this.mountIntersectedSliders.bind(this), this.options);
    }
    mountIntersectedSliders(entries, observer) {
        entries.forEach(entry => {
            if (!entry.isIntersecting) {
                return;
            }
            let target = entry.target;
            this.sliders.forEach((slider) => {
                if (slider.getElement() !== target) {
                    return;
                }
                slider.lazyMount();
            });
            observer.unobserve(target);
        });
    }
    addSlider(slider) {
        if (!this.intersectionObserver) {
            slider.lazyMount();
            return;
        }
        this.sliders.push(slider);
        this.intersectionObserver.observe(slider.getElement());
    }
}
let lazyInit = new LazyInit();
class Slider {
    constructor(element, settings) {
        this.element = element;
        this.settings = settings;
        this.slider = new Splide(element, settings.getSplideSettings());
    }
    isHaveToInit() {
        let countOfItems = this.element.querySelectorAll('.splide__item').length;
        let multiplyQuantity = this.settings.getCurrentSliderSettings().multiplyQuantity;
        return (!multiplyQuantity ||
            !!(countOfItems % multiplyQuantity));
    }
    // with css will solve the different slides height issue
    updateSlideHeightAccordingToSlide(slideIndex) {
        let index = slideIndex < 10 ?
            '0' + slideIndex :
            slideIndex.toString();
        let id = this.element.getAttribute('id');
        let nextSlide = this.element.querySelector('#' + id + '-slide' + index);
        this.element.querySelector('.splide__list').style.height = nextSlide.scrollHeight + 'px';
    }
    goToNextSlide() {
        this.slider.go('>');
    }
    goToPrevSlide() {
        this.slider.go('<');
    }
    // from 0
    goToSlide(slideNumber) {
        this.slider.go(slideNumber);
    }
    addInitListener(callback) {
        this.slider.on('mounted', callback);
    }
    mount(isLazy = true) {
        isLazy ?
            lazyInit.addSlider(this) :
            this.lazyMount();
    }
    lazyMount() {
        this.slider.on('mounted', () => {
            if (!this.settings.getCurrentSliderSettings().isAdaptiveHeight) {
                return;
            }
            this.updateSlideHeightAccordingToSlide(1);
        });
        // don't skip mount, even if should destroy later
        // because should be initialized, like '.is-initialized' class and display items, etc..
        this.slider.mount();
        this.slider.on('move', (newIndex, oldIndex, destIndex) => {
            if (!this.settings.getCurrentSliderSettings().isAdaptiveHeight) {
                return;
            }
            this.updateSlideHeightAccordingToSlide(newIndex + 1);
        });
        if (!this.isHaveToInit()) {
            this.slider.destroy(true);
            this.element.classList.add('an-skipped');
        }
    }
    getElement() {
        return this.element;
    }
}
window['_Slider'] = {
    Slider: Slider,
    Settings: Settings,
};
