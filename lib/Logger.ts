import Log from '@lightsourcee/log'

declare global {
    interface Window {
        _Logger: typeof Log,
    }
}

window['_Logger'] = Log
