import {
    attr,
    autoShadowRoot,
    bind,
    controller,
    defineObservedAttributes,
    initializeAttrs,
    target,
    targets,
} from '@github/catalyst'

// https://github.github.io/catalyst/guide/introduction/
class Catalyst extends HTMLElement {

    // can be used if an element has another name than a class
    public static manualDefine(elementName: string, className: any): void {
        defineObservedAttributes(className)
        window.customElements.define(elementName, className)
    }

    // can be used if an element has another name than a class
    protected manualBind(): void {
        autoShadowRoot(this)
        initializeAttrs(this)
        bind(this)
    }

    public connectedCallback(): void {
        'loading' === document.readyState ?
            document.addEventListener('DOMContentLoaded', this.setup.bind(this))
            : this.setup()
    }

    public setup(): void {

    }
}

declare global {
    interface Window {
        _Catalyst: {
            controller: any,
            target: any,
            targets: any,
            attr: any,
            class: typeof Catalyst,
        },
    }
}

window['_Catalyst'] = {controller, target, targets, attr, class: Catalyst}
