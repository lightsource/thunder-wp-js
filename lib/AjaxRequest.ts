import dependency from "./Dependency";

let logger = window._Logger

dependency.exists(logger, '_Logger')

interface ResponseInterface {
    isSuccessful: boolean,
    errors: string,
    html: string
}

interface AjaxResponseHandlerInterface {
    (response: string): void,
}

class Setting {
    public url: string = ''
    public name: string = ''
    public type: string = 'POST'
    public isCustomResponse: boolean = false
}

class AjaxRequest {
    private settings: Setting

    public constructor(settings: Setting) {
        this.settings = settings
    }

    protected parseResponse(response: string): ResponseInterface {
        let result: ResponseInterface = null

        try {
            result = JSON.parse(response)
            if (!result.hasOwnProperty('isSuccessful') ||
                !result.hasOwnProperty('errors') ||
                !result.hasOwnProperty('html')) {
                new Error('Properties are missing')
            }
        } catch (exception) {
            logger.write(logger.level.ERROR, 'Fail to parse JSON', {
                error: exception,
                response: response,
            })

            new Error('Fail to parse json')
        }

        return result
    }

    protected processResponse(request: XMLHttpRequest, responseHandler: AjaxResponseHandlerInterface): void {
        if (request.readyState !== 4) {
            return
        }

        if (200 !== request.status) {
            logger.write(logger.level.ERROR, 'Response status is wrong', {
                request: request,
            })

            new Error('Response status is wrong')
        }

        if (this.settings.isCustomResponse) {
            !responseHandler || responseHandler(request.responseText)
            return
        }

        let response = this.parseResponse(request.responseText)

        if (!response.isSuccessful) {
            logger.write(logger.level.ERROR, 'Request was wrong', {
                response: response,
            })

            new Error('Request was wrong')
        }

        !responseHandler || responseHandler(response.html)
    }

    public make(data: object = null, responseHandler: AjaxResponseHandlerInterface = null): void {

        let formData = new FormData()

        !this.settings.name || formData.append('action', this.settings.name)
        formData.append('_noCache', new Date().getTime().toString())

        if (data) {
            for (let property in data) {
                if (!data.hasOwnProperty(property)) {
                    continue
                }

                formData.append(property.toString(), data[property].toString())
            }
        }

        const request = new XMLHttpRequest()

        request.timeout = 10000
        request.open(this.settings.type, this.settings.url, true)
        request.addEventListener('readystatechange', this.processResponse.bind(this, request, responseHandler))
        request.addEventListener('timeout', () => {
            throw new Error('Ajax timeout')
        })

        request.send(formData)
    }
}

declare global {
    interface Window {
        _AjaxRequest: {
            Settings: typeof Setting,
            Request: typeof AjaxRequest,
        },
    }
}

window['_AjaxRequest'] = {
    Settings: Setting,
    Request: AjaxRequest,
}

