import { attr, autoShadowRoot, bind, controller, defineObservedAttributes, initializeAttrs, target, targets, } from '@github/catalyst';
// https://github.github.io/catalyst/guide/introduction/
class Catalyst extends HTMLElement {
    // can be used if an element has another name than a class
    static manualDefine(elementName, className) {
        defineObservedAttributes(className);
        window.customElements.define(elementName, className);
    }
    // can be used if an element has another name than a class
    manualBind() {
        autoShadowRoot(this);
        initializeAttrs(this);
        bind(this);
    }
    connectedCallback() {
        'loading' === document.readyState ?
            document.addEventListener('DOMContentLoaded', this.setup.bind(this))
            : this.setup();
    }
    setup() {
    }
}
window['_Catalyst'] = { controller, target, targets, attr, class: Catalyst };
